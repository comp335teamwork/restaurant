### Setup
-[ ] Configure a dev and prod MYSQL RDBMS

-[ ] Copy `example-restaurant.properties` into `restaurant.properties`

-[ ] Copy `example-restaurant.properties` into `test-restaurant.properties`

-[ ] Fill out `restaurant.properties` with prod credentials and `test-restaurant.properties`
 with dev credentials.

-[ ] Run tests and all tests should pass.

