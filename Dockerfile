FROM ubuntu:16.04

RUN apt-get update && \
    apt-get -y install default-jre-headless && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app
ADD target/restaurant-*.jar /app

EXPOSE 8080:8080

CMD java -jar restaurant-*.jar

