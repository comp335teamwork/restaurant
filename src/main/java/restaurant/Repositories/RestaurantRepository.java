package restaurant.Repositories;

import org.springframework.data.repository.CrudRepository;
import restaurant.Tables.Restaurant;

public interface RestaurantRepository extends CrudRepository<Restaurant, Long> {
    Restaurant findByRestaurantToken(String userToken);

}
