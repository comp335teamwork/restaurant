package restaurant.Repositories;

import restaurant.Tables.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUserToken(String userToken);
}
