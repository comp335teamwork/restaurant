package restaurant.Tables;

import javax.persistence.*;
import java.util.*;

@Entity
public class Restaurant {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    public String restaurantToken;
    private String menuToken;
    private String description;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "User.id")
    private List<User> employees;

    public Restaurant(){

    }

    public Restaurant(String name, String description, List<User> employees){
        this.name = name;
        this.employees = employees;
        this.description = description;
        this.restaurantToken = "restaurant-" + UUID.randomUUID().toString();
    }

    public void bindMenu(String menuToken){
        this.menuToken = menuToken;
    }

    public boolean isEmployee(String userToken) {
        for (User employee : employees) {
            if (employee.userToken.equals(userToken)) {
                return true;
            }
        }
        return false;
    }

    public Map<String, Object> serialize(){
        Map<String, Object> serialized = new HashMap<>();
        serialized.put("restaurant-token", restaurantToken);
        serialized.put("menu-token", menuToken);
        serialized.put("description", description);
        serialized.put("name", name);
        return serialized;
    }
}
