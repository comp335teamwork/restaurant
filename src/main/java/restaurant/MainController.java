package restaurant;

import org.springframework.web.bind.annotation.*;
import restaurant.Repositories.RestaurantRepository;
import restaurant.Repositories.UserRepository;
import restaurant.Tables.User;
import restaurant.Tables.Restaurant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
class MainController {
    private final UserRepository userRepository;
    private final RestaurantRepository restaurantRepository;

    @Autowired
    public MainController(UserRepository userRepository, RestaurantRepository restaurantRepository) {
        this.userRepository = userRepository;
        this.restaurantRepository = restaurantRepository;
    }

    @PostMapping(path = "/create-restaurant")
    public @ResponseBody
    Map<String, String> createRestuarant(@RequestBody Map<String, Object> request) {
        List<Object> employeesResponse = (List<Object>) request.get("employees");
        String restaurantName = (String) request.get("restaurant-name");
        String restaurantDescription = (String) request.get("restaurant-description");
        List<User> employees = new ArrayList<>();
        for (Object userResponse : employeesResponse) {
            Map<String, Object> jsonUsers = (Map<String, Object>) userResponse;
            String userToken = (String) jsonUsers.get("user-token");
            User user = userRepository.findByUserToken(userToken);
            if (userRepository.findByUserToken(userToken) == null) {
                user = new User(userToken);
                userRepository.save(user);
            }
            employees.add(user);
        }
        Restaurant restaurant = new Restaurant(restaurantName, restaurantDescription, employees);
        restaurantRepository.save(restaurant);

        Map<String, String> response = new HashMap<>();
        response.put("restaurant-token", restaurant.restaurantToken);
        return response;
    }

    @PostMapping(path = "/bind-menu")
    public @ResponseBody
    String bindMenuToRestaurant(@RequestBody Map<String, Object> request) {
        String restaurantToken = (String) request.get("restaurant-token");
        Restaurant restaurant = restaurantRepository.findByRestaurantToken(restaurantToken);

        String menuToken = (String) request.get("menu-token");
        restaurant.bindMenu(menuToken);
        restaurantRepository.save(restaurant);
        return "Success";
    }

    @GetMapping(path = "/list-all")
    public @ResponseBody
    Map<String, List<Map>> listRestaurants() {
        List<Restaurant> restaurants = (List<Restaurant>) restaurantRepository.findAll();
        Map<String, List<Map>> response = new HashMap<>();
        List<Map> restaurantsSerialized = new ArrayList<>();
        for (Restaurant restaurant : restaurants) {
            restaurantsSerialized.add(restaurant.serialize());
        }
        response.put("restaurant", restaurantsSerialized);

        return response;
    }

    @PostMapping(path = "/get-restaurant")
    public @ResponseBody
    Map<String, Map> getRestaurant(@RequestBody Map<String, Object> request) {
        Restaurant restaurant = restaurantRepository.findByRestaurantToken(
                (String) request.get("restaurant-token")
        );
        Map<String, Map> response = new HashMap<>();

        response.put("restaurant", restaurant.serialize());

        return response;
    }

    @PostMapping(path = "/is-employee")
    public @ResponseBody
    Map<String, Boolean> checkIsEmployee(@RequestBody Map<String, Object> request) {
        String restaurantToken = (String) request.get("restaurant-token");
        Restaurant restaurant = restaurantRepository.findByRestaurantToken(restaurantToken);

        String userToken = (String) request.get("user-token");
        Map<String, Boolean> response = new HashMap<>();
        if (restaurant.isEmployee(userToken)) {
            response.put("employee-status", true);
        } else {
            response.put("employee-status", false);
        }

        return response;
    }
}
